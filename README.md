# hello-python

This is a reworked clone of a "Hello World!" Python3 app that uses Flask. I've modified it to test Kubernetes and Docker. It was originally taken from: https://github.com/JasonHaley/hello-python.git and was used as an example on kubernetes.io.